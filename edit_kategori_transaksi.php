<!DOCTYPE html>
<html>
	<head>
		<title>Edit Kategori Transaksi</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_kategori_transaksi.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_kategori_transaksi.php">Kategori Transaksi</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Kategori Transaksi</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					if(isset($_GET['id_kategori']))
					{
						$id_kategori = $_GET['id_kategori'];
						//mencegah sql injection
						if(is_numeric($id_kategori))
						{
							$query = "select * from kategori_transaksi where id_kategori=$id_kategori";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								
								$nama_kategori = $hasil['nama_kategori'];
								
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID Kategori : </label> <input type=\"text\" name=\"id_kategori\" value=\"$id_kategori\" readonly/><br>";	
								//membuat input yang bisa diedit2
								//nama kategori
								echo "<label class=\"frm\">Nama Kategori : </label> <input type=\"text\" name=\"nama_kategori\" value=\"$nama_kategori\"/><br>";

								echo "<input type=\"hidden\" name=\"tabel\" value=\"kategori_transaksi\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>