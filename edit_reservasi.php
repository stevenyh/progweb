<!DOCTYPE html>
<html>
	<head>
		<title>Edit Reservasi</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master_guest.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_reservasi.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_reservasi.php">Reservasi</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Reservasi</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					if(isset($_GET['id_reservasi']))
					{
						$id_reservasi = $_GET['id_reservasi'];
						//mencegah sql injection
						if(is_numeric($id_reservasi))
						{
							$query = "select * from reservasi where id_reservasi=$id_reservasi";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								
								$id_karyawan = $hasil['id_karyawan'];
								$id_pelanggan = $hasil['id_pelanggan'];
								$waktu_pesan = $hasil['waktu_pesan'];
								$waktupakai = $hasil['waktupakai'];
								$keterangan = $hasil['keterangan'];
								
								
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID Reservasi : </label> <input type=\"text\" name=\"ID_reservasi\" value=\"$id_reservasi\" readonly/><br>";	
								//membuat input yang bisa diedit2

								//ID_karyawan
								echo "<label class=\"frm\">ID Karyawan : </label>";
								echo "<select name=\"id_karyawan\">";
								echo "<option disabled selected value> -- select an option -- </option>";
								$query2 = "Select id_karyawan,nama from karyawan";

								$result2 = mysqli_query($kon, $query2);
								$hasil2 = array();
								while($row = mysqli_fetch_assoc($result2))
								{
									$hasil2[] = $row;
								}

								$counter2 = 0;
								foreach($hasil2 as $baris)
								{
																	
									$temp1 = $baris['id_karyawan'];
									$temp2 = $baris['nama'];

									if($id_karyawan == $temp1)
									{

										echo "<option value=\"$temp1\" selected=\"selected\">$temp1 - $temp2</option>";
									}
									else
									{
										echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
									}
									$counter2 += 1;
																	
								}
								echo "</select><br>";
								unset($query2);
								unset($result2);
								unset($hasil2);
								unset($counter2);

								//id_pelanggan
								echo "<label class=\"frm\">ID Pelanggan : </label>";
								echo "<select name=\"id_pelanggan\">";
								echo "<option disabled selected value> -- select an option -- </option>";
								$query2 = "Select id_pelanggan,nama from pelanggan";

								$result2 = mysqli_query($kon, $query2);
								$hasil2 = array();
								while($row = mysqli_fetch_assoc($result2))
								{
									$hasil2[] = $row;
								}

								$counter2 = 0;
								foreach($hasil2 as $baris)
								{
																	
									$temp1 = $baris['id_pelanggan'];
									$temp2 = $baris['nama'];

									if($id_pelanggan == $temp1)
									{

										echo "<option value=\"$temp1\" selected=\"selected\">$temp1 - $temp2</option>";
									}
									else
									{
										echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
									}
									$counter2 += 1;
																	
								}
								echo "</select><br>";
								unset($query2);
								unset($result2);
								unset($hasil2);
								unset($counter2);

								//waktu_pesan
								echo "<label class=\"frm\">Waktu Pesan : </label>";
								$newdate = date('Y-m-d\TH:i', strtotime($waktu_pesan));
								echo "<input type=\"datetime-local\" name=\"waktu_pesan\" value=\"$newdate\"/><br>";
								unset($newdate);

								//waktupakai
								echo "<label class=\"frm\">Waktu Pakai : </label>";
								$newdate = date('Y-m-d\TH:i', strtotime($waktupakai));
								echo "<input type=\"datetime-local\" name=\"waktupakai\" value=\"$newdate\"/><br>";
								unset($newdate);

								//keterangan
								echo "<label class=\"frm\">Keterangan : </label> <input type=\"text\" name=\"keterangan\" value=\"$keterangan\"/><br>";

								echo "<input type=\"hidden\" name=\"tabel\" value=\"reservasi\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>