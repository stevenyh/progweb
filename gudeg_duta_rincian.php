<!DOCTYPE html>
<html>
	<head>
		<title>Daftar Rincian</title> <!-- diganti sesuai nama tabel -->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master_guest.html";
		?>
		<div class="content">
			
			<div id="breadcrumb" style="margin-left:0px; width: 95%">

				<a href="javascript:window.location.href=window.location.href">Rincian</a>   <!-- diganti -->
				
			</div>
			<br>
			<div id="isi">
				<h2>Daftar Rincian</h2> <!-- diganti sesuai nama tabel -->
				<a href="tambah_rincian.php"><span class="btn">Tambah</span></a>
				<table>
				<thead>
					<tr>
						<td>ID Rincian</td> <!-- diganti sesuai nama kolom tabel -->
						<td>ID Penjualan</td> <!-- diganti sesuai nama kolom tabel -->
						<td>ID Makanan</td> <!-- diganti sesuai nama kolom tabel -->
						<td>Kuantitas</td> <!-- diganti sesuai nama kolom tabel -->
						<td></td>
					</tr>
				</thead>
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					
						
					$query = "select * from rincian"; //diganti sesuai nama tabel
					$result = mysqli_query($kon, $query);
					$hasil = array();
					while($row = mysqli_fetch_assoc($result))
					{
						$hasil[] = $row;
					}
					$idtabel = -99;
					foreach($hasil  as $baris)
					{
						echo "<tr>";
						$kolomke = 0;
						$id = $baris['id_rincian'];
						foreach($baris as $kolom)
						{
							if($kolomke == 2){
								$query2 = "select nama from makanan where id_makanan='$kolom'";
								$result2 = mysqli_query($kon, $query2);
								$row2 = mysqli_fetch_assoc($result2);
								echo "<td>". $kolom .' - ' . $row2['nama'] . "</td>";
							}
							else
							{
								echo "<td>" . $kolom . "</td>";
							}
							if($idtabel == -99)
							{
								$idtabel = $kolom;
							}
							$kolomke += 1;
						}
						echo "<td><a href=\"edit_rincian.php?id_rincian=$id\"><img src=\"edit.png\" width=20 height=20 /></a><a class=\"ahapus\"  href=\"hapus.php?id_rincian=$id\"><img src=\"iDelete.png\" width=20 height=20/></a></td>"; 
						//edit_presensi.php DIGANTI NAMA edit_(tabel).php?(NAMA KOLOM PK)=$idtabel
						
						echo "</tr>";
					}	
					

					
				?>	
			</div>
			
			
		</div>
	</div>
	
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>