<!DOCTYPE html>
<html>
	<head>
		<title>Pelanggan</title> <!-- diganti sesuai nama tabel -->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master.html";
		?>
		<div class="content">
			
			<div id="breadcrumb" style="margin-left:0px; width: 95%">

				<a href="javascript:window.location.href=window.location.href">Pelanggan</a>   <!-- diganti -->
				
			</div>
			<br>
			<div id="isi">
				<h2>Daftar Pelanggan</h2> <!-- diganti sesuai nama tabel -->
				<a href="tambah_pelanggan.php"><span class="btn">Tambah</span></a>
				<table>
				<thead>
					<tr>
						<td>ID Pelanggan</td> <!-- diganti sesuai nama kolom tabel -->
						<td>Nama</td> <!-- diganti sesuai nama kolom tabel -->
						<td>No. Hp</td> <!-- diganti sesuai nama kolom tabel -->
						<td>Member</td> <!-- diganti sesuai nama kolom tabel -->
						<td></td>
					</tr>
				</thead>
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					
						
					$query = "select * from pelanggan"; //diganti sesuai nama tabel
					$result = mysqli_query($kon, $query);
					$hasil = array();
					while($row = mysqli_fetch_assoc($result))
					{
						$hasil[] = $row;
					}
					$idtabel = -99;
					foreach($hasil  as $baris)
					{
						echo "<tr>";
						$id = $baris['id_pelanggan'];
						foreach($baris as $kolom)
						{
							echo "<td>" . $kolom . "</td>";
							if($idtabel == -99)
							{
								$idtabel = $kolom;
							}
						}
						echo "<td><a href=\"edit_pelanggan.php?id_pelanggan=$id\"><img src=\"edit.png\" width=20 height=20 /></a><a href=\"hapus.php?id_pelanggan=$id\" class=\"ahapus\"><img src=\"iDelete.png\" width=20 height=20/></a></td>"; 
						//edit_presensi.php DIGANTI NAMA edit_(tabel).php?(NAMA KOLOM PK)=$idtabel
						
						echo "</tr>";
					}	
					

					
				?>	
			</div>
			
			
		</div>
	</div>
	
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>