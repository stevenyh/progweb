<!DOCTYPE html>
<html>
	<head>
		<title>Edit Absen</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master_guest.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_absen.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_absen.php">Absen</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Absen</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					if(isset($_GET['id_absen']))
					{
						$id_absen = $_GET['id_absen'];
						//mencegah sql injection
						if(is_numeric($id_absen))
						{
							$query = "select * from absen where id_absen=$id_absen";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								
								$keterangan = $hasil['keterangan'];
								
								
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID Absen : </label> <input type=\"text\" name=\"id_absen\" value=\"$id_absen\" readonly/><br>";	
								//membuat input yang bisa diedit2
								//keterangan
								echo "<label class=\"frm\">Keterangan : </label> <input type=\"text\" name=\"keterangan\" value=\"$keterangan\"/><br>";

								echo "<input type=\"hidden\" name=\"tabel\" value=\"absen\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
<?php
		require "tutupkoneksi.php";
	?>
</html>