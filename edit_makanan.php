<!DOCTYPE html>
<html>
	<head>
		<title>Edit Makanan</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_makanan.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_makanan.php">Makanan</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Makanan</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					if(isset($_GET['id_makanan']))
					{
						$id_makanan = $_GET['id_makanan'];
						//mencegah sql injection
						if(is_numeric($id_makanan))
						{
							$query = "select * from makanan where id_makanan=$id_makanan";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								$id_kategori = $hasil['id_kategori'];
								$nama = $hasil['nama'];
								$harga = $hasil['harga'];
								
								
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID Makanan : </label> <input type=\"number\" name=\"id_makanan\" value=\"$id_makanan\" readonly/><br>";	
								//membuat input yang bisa diedit2
								//id kategori
								echo "<label class=\"frm\">ID Kategori : </label>";
								echo "<select>";
								echo "<option disabled selected value> -- select an option -- </option>";
								$query2 = "Select id_kategori,keterangan from kategori_makanan";

								$result2 = mysqli_query($kon, $query2);
								$hasil2 = array();
								while($row = mysqli_fetch_assoc($result2))
								{
									$hasil2[] = $row;
								}

								$counter2 = 0;
								foreach($hasil2 as $baris)
								{
																	
									$temp1 = $baris['id_kategori'];
									$temp2 = $baris['keterangan'];

									if($id_kategori == $temp1)
									{

										echo "<option value=\"$counter2\" selected=\"selected\">$temp1 - $temp2</option>";
									}
									else
									{
										echo "<option value=\"$counter2\" >$temp1 - $temp2</option>";
									}
									$counter2 += 1;
																	
								}
								echo "</select><br>";
								unset($query2);
								unset($result2);
								unset($hasil2);
								unset($counter2);

								//nama
								echo "<label class=\"frm\">Nama : </label> <input type=\"text\" name=\"nama\" value=\"$nama\"/><br>";

								//harga
								echo "<label class=\"frm\">Harga : </label> <input type=\"number\" name=\"harga\" value=\"$harga\"/><br>";

								echo "<input type=\"hidden\" name=\"tabel\" value=\"makanan\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>