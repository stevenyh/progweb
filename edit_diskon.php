<!DOCTYPE html>
<html>
	<head>
		<title>Edit Diskon</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_diskon.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_diskon.php">Diskon</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Diskon</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					if(isset($_GET['id_diskon']))
					{
						$id_diskon = $_GET['id_diskon'];
						//mencegah sql injection
						if(is_numeric($id_diskon))
						{
							$query = "select * from diskon where id_diskon=$id_diskon";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								
								$diskon = $hasil['diskon'];
								$keterangan = $hasil['keterangan'];
								
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID Diskon : </label> <input type=\"text\" name=\"id_diskon\" value=\"$id_diskon\" readonly/><br>";	
								//membuat input yang bisa diedit2
								//diskon
								echo "<label class=\"frm\">Diskon : </label> <input type=\"number\" name=\"diskon\" value=\"$diskon\"/><br>";

								//keterangan
								echo "<label class=\"frm\">Keterangan : </label> <input type=\"text\" name=\"keterangan\" value=\"$keterangan\"/><br>";

								echo "<input type=\"hidden\" name=\"tabel\" value=\"Diskon\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>