<!DOCTYPE html>
<html>
	<head>
		<title>Tambah reservasi</title> <!--ganti nama tabel-->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master_guest.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_reservasi.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_reservasi.php">reservasi</a> >  <a href="javascript:window.location.href=window.location.href">Tambah</a>  <!--ganti nama tabel-->
				
			</div>
			<br>
			<div id="isi">
				<h2>Tambah reservasi</h2> <!--ganti nama tabel-->
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					//membuat input yang bisa diedit2
					echo "<form action=\"tambah.php\" method=\"post\">";

//id_karyawan
					echo "<label class=\"frm\">ID Karyawan : </label>";
					echo "<select name=\"id_karyawan\">";
					echo "<option disabled selected value> -- select an option -- </option>";
					$query2 = "Select id_karyawan,nama from karyawan";

					$result2 = mysqli_query($kon, $query2);
					$hasil2 = array();
					while($row = mysqli_fetch_assoc($result2))
					{
						$hasil2[] = $row;
					}

					$counter2 = 0;
					foreach($hasil2 as $baris)
					{
														
						$temp1 = $baris['id_karyawan'];
						$temp2 = $baris['nama'];

						
						echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
						
						$counter2 += 1;
														
					}
					echo "</select><br>";
					unset($query2);
					unset($result2);
					unset($hasil2);
					unset($counter2);

//id_pelanggan
					echo "<label class=\"frm\">ID Pelanggan : </label>";
					echo "<select name=\"id_pelanggan\">";
					echo "<option disabled selected value> -- select an option -- </option>";
					$query2 = "Select id_pelanggan,nama from pelanggan";

					$result2 = mysqli_query($kon, $query2);
					$hasil2 = array();
					while($row = mysqli_fetch_assoc($result2))
					{
						$hasil2[] = $row;
					}

					$counter2 = 0;
					foreach($hasil2 as $baris)
					{
														
						$temp1 = $baris['id_pelanggan'];
						$temp2 = $baris['nama'];

						
						echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
						
						$counter2 += 1;
														
					}
					echo "</select><br>";
					unset($query2);
					unset($result2);
					unset($hasil2);
					unset($counter2);

//waktu_pesan
echo "<label class=\"frm\">Waktu Pesan : </label>";
echo "<input type=\"datetime-local\" name=\"waktu_pesan\" value=\"\"/><br>";


//waktupakai
echo "<label class=\"frm\">Waktu Pakai : </label>";
echo "<input type=\"datetime-local\" name=\"waktupakai\" value=\"\"/><br>";


//keterangan
					echo "<label class=\"frm\">Keterangan : </label> <input type=\"text\" name=\"keterangan\" value=\"\"/><br>";

//input hidden
					echo "<input type=\"hidden\" name=\"tabel\" value=\"reservasi\"><br>"; //ganti value
					echo"<input type=\"submit\" value=\"Tambah\"/>";

					echo "</form>"
						
				?>	
			</div>
			
			
		</div>
	</div>
	
	</body>
	<?php
mysqli_close($kon);
?>
</html>

