<!DOCTYPE html>
<html>
	<head>
		<title>LAPORAN MAKANAN</title> <!--ganti nama tabel-->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script src="jquery-3.3.1.min.js"></script>
	</head>
	<body>
		<?php
			require "master.html";
			require "koneksi.php";
			
			
		?>
		<div class="content">
			
			<br>
			<div id="isi">
				<center><h2 >LAPORAN MAKANAN</h2>

					<br>
					<br>

					<div id="d1">
						<label>Laporan tanggal</label>
						<input type="date" id="date1" /><label> - </label><input type="date" id="date2"/>
						 <br><br>
						
					</div>
				</center> 
				<br>
				<br>
				
				<table id="hasil">

				</table>
				
				
			</div>
			
			
		</div>
	</div>
		<table id="laporan">

			
		</table>
	</body>


	<!-- JAVASCRIPT UTNUK CANVAS & AJAX UNTUK TABLE -->
	<script>
		
		
		addEventListener("change", function(){generatetabel()});
		function generatetabel(){
			var lihatbedasarkanvalue = 'tanpa';
			var batas1 = document.getElementById('date1').value;
			var batas2 = document.getElementById('date2').value;
			batas2= addDays(batas2,1);
			var batas1str = ubahkeformatbenar(batas1);
			var batas2str = ubahkeformatbenar(batas2);
			if(batas1str.length > 1 && batas2str.length > 1)
			{
				//alert(batas1str + " : " + batas2str);
				$.ajax({
				    type: "POST",
				    url: 'laporan_makanan_cari.php',
				    data: {batas1: batas1str, batas2: batas2str, lihatbedasarkan:lihatbedasarkanvalue},
				    success: function(data){
				        $("#hasil").html(data);
				    }
				});
			}


			
		}



		

		//mendapatkan penjualanmin dan penjualanmax
		function hariini(){
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();

			if(dd<10) {
			    dd = '0'+dd;
			} 

			if(mm<10) {
			    mm = '0'+mm;
			} 

			today = tambahnoldate(yyyy) + '-' + tambahnoldate(mm) + '-' + tambahnoldate(dd);

			return today;
		}
		function ubahkeformatbenar(x){
			var today = new Date(x);
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();

			if(dd<10) {
			    dd = '0'+dd;
			} 

			if(mm<10) {
			    mm = '0'+mm;
			} 

			today = tambahnoldate(yyyy) + '-' + tambahnoldate(mm) + '-' + tambahnoldate(dd);

			return today;
		}
		//fungsi untuk mengubah yyyy-mm-dd jam-menit-detik ke yyyy-mm-dd
		function ubahkeyyyymmdd(str){
			return str.substring(0,str.indexOf(" "));
		}

		//fungsi untuk memperbaiki 4-5-2017 menjadi 04-05-2017
		function tambahnoldate(x)
		{
			str = x.toString();
			if (str.length  == 1)
			{
				str = "0" + str;
			}
			else
			{
				//alert("str length != 1 -> " + str + " length : " + str.length);
			}
			return str;
		}
		//fungsi untuk menambah date
		function addDays(date, days) {
		  var result = new Date(date);
		  result.setDate(result.getDate() + days);
		  var dd = result.getDate();
		  var mm = result.getMonth() + 1;
		  var y = result.getFullYear();


		  var someFormattedDate = tambahnoldate(y) + '-' + tambahnoldate(mm) + '-' + tambahnoldate(dd);
		  //alert(tambahnoldate(mm));
		  return someFormattedDate;
		}
		//cek koordinat mouse
		
		
	</script>
	<?php
		require "tutupkoneksi.php";
	?>
</html>
