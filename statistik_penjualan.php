<!DOCTYPE html>
<html>
	<head>
		<title>Beranda</title> <!--ganti nama tabel-->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script src="jquery-3.3.1.min.js"></script>
	</head>
	<body>
		<?php
			require "master.html";
			require "koneksi.php";
			$query = "select date_format(tanggal,\"%Y-%m-%d\") as tanggal, sum(total) as total from nota_penjualan where cast(tanggal as datetime) <= NOW() GROUP BY CAST(tanggal AS DATE) order by cast(tanggal as datetime) asc;";
			
			$result  = mysqli_query($kon, $query);
			if(!$result)
			{
				echo mysqli_error($kon);
			}
			$hasil = array();
			$length = 0;
			$idx = 0;
			while($row = mysqli_fetch_assoc($result))
			{
				$length += 1;
				$hasil[] = $row;
			}
			echo $length;
			print_r($hasil);
			
		?>
		<div class="content">
			
			<br>
			<div id="isi">
				<center><h2 >STATISTIK PENJUALAN</h2>

					<br>
					<br>

					<div id="d1">
						<label>Laporan tanggal</label>
						<input type="date" id="date1" /><label> - </label><input type="date" id="date2"/>
						 <br><br>
						<canvas id="canvas" style="background-color: white; border:1px solid black" width="680" height="300">

						</canvas>
					</div>
				</center> 
				<br>
				<br>
				<label>Lihat Bedasarkan</label>
				<select id="lb">
					<option disabled selected value> -- select an option -- </option>
					<option value="tanpa">Tampilkan Semua</option>
					<option value="hari">harian</option>
					<option value="bulan">bulanan</option>
					<option value="tahun">tahunan</option>
				</select>
				<br>
				<br>
				<table id="hasil">
					
				</table>
				
				
			</div>
			
			
		</div>
	</div>
		<table id="laporan">

			
		</table>
	</body>


	<!-- JAVASCRIPT UTNUK CANVAS & AJAX UNTUK TABLE -->
	<script>
		
		
		addEventListener("change", function(){generatetabel()});
		function generatetabel(){
			var lihatbedasarkanvalue = document.getElementById('lb').value;
			var batas1 = document.getElementById('date1').value;
			var batas2 = document.getElementById('date2').value;
			batas2= addDays(batas2,1);
			var batas1str = ubahkeformatbenar(batas1);
			var batas2str = ubahkeformatbenar(batas2);
			if(batas1str.length > 1 && batas2str.length > 1)
			{
				//alert(batas1str + " : " + batas2str);
				$.ajax({
				    type: "POST",
				    url: 'statistik_penjualan_cari.php',
				    data: {batas1: batas1str, batas2: batas2str, lihatbedasarkan:lihatbedasarkanvalue},
				    success: function(data){
				        $("#hasil").html(data);
				    }
				});
			}


			
		}


		//membuat variabel untuk canvas
		var canvas = document.getElementById('canvas');
		var ctx = canvas.getContext('2d');

		//membuat variabel untuk menampung hasil query
		var penjualan = [[],[]];
		var hasilpenjualan = [[],[]];
		//console.log(hasilpenjualan);
		var length = <?php echo $length;?>;
		var hasilength = 0;


		penjualan = <?php echo json_encode($hasil, JSON_PRETTY_PRINT) ?>;
		
		var tglawal = penjualan[0]['tanggal'];
		var c = 0;
		
		while(c != length)
		{
			if((penjualan[c]['tanggal']) != tglawal)
			{
				var isikosong = [];
				isikosong['tanggal'] = tglawal;
				isikosong['total'] = 0;
				//console.log((penjualan[i]['tanggal']) + " : " + tglawal);
				length += 1;
				penjualan.splice(c,0,isikosong);
			}

			tglawal = addDays(tglawal, 1);
			c += 1;
		}
		
		while(penjualan[length - 1]['tanggal'] != hariini())
		{
			var isikosong = [];
			isikosong['tanggal'] = addDays(penjualan[length - 1]['tanggal'], 1);
			isikosong['total'] = 0;
			//console.log((penjualan[i]['tanggal']) + " : " + tglawal);
			length += 1;
			penjualan.splice(length,0,isikosong);
		}
		console.log(penjualan);
		//pembuatan variabel maxtotal dan mintotal
		var maxtotal = 0;
		var mintotal = 99999999;

		

		//membuat variabel titik yang digunakan sebagai titik penjualan pada canvas
		var titik = [];

		


		//membuat eventlistener untuk mouse
		//mx dan my adalah hasil posisi mouse
		canvas.addEventListener('mousemove',hovermouse , false);
		mx = 0;
		my = 0;

		

		//jalankan animasi
		setInterval(grafik, 500/15);

		//titik x : (600/hasillength) + 20
		//flexible

		//titik y : (50-250)
		//50
		//100
		//150
		//200
		//250

		

		function grafik(){
			//getdate1();
			ctx.clearRect(0,0,canvas.width,canvas.height);
			
			generatehasilpenjualan(getdate1(), getdate2());
			cariminmax();
			buatgarisgrid();
			buatgarisgrafik();
			buatbulatangrafik();
			buattanggaldibawah();
			saatdihover();
			
		}
		//cari min dan max 
		function cariminmax(){
			maxtotal = 0;
			mintotal = 99999999;
			for(var i = 0;i<hasillength;i++)
			{
				var tamp = parseInt(hasilpenjualan[i]["total"]);
				//console.log("tamp : " + tamp);
				if(tamp > maxtotal)
				{
					maxtotal = tamp;
				}
				if(tamp < mintotal)
				{
					mintotal = tamp;
				}

				
			}
		}
		//mendapatkan value dari input date1
		function getdate1(){
			var x = document.getElementById('date1').value;
			return x;
		}
		function getdate2(){
			var x = document.getElementById('date2').value;
			return x;
		}
		//megenerate hasilpenjualan bedasarkan tanggal
		function generatehasilpenjualan(tgl1, tgl2)
		{

			hasilength = 0;
			hasilpenjualan = [[],[]];
			var strtgl1 = String(tgl1);
			var strtgl2 = String(tgl2);
			var start = false;
			var idx = 0;

			
			//SAMPAI DISINI ERROR, TGL 1 < PENJUALAN[0]['TANGGAL']
			var datetgl1 = new Date(strtgl1);
			var datetgl2 = new Date(strtgl2);
			var dateawal = new Date(penjualan[0]['tanggal']);
			
			//jika tgl1 <  penjualan[0]['tanggal']
			var tempi = 0;
			while (datetgl1.getTime() < dateawal.getTime())
			{
				//alert("datetgl1 : " + datetgl1 + " dateawal : " + dateawal);
				var isikosong = [];
				isikosong['tanggal'] = ubahkeformatbenar(datetgl1.toString());
				isikosong['total'] = 0;
				hasilpenjualan.splice(tempi,0,isikosong);
				tempi += 1;
				//alert("datetgl1 : " + datetgl1 + " kalau ditambah : " + setDate(datetgl1.getDate() + 1));
				datetgl1.setDate(datetgl1.getDate() + 1);
				//alert("hasil datetgl1 : " + datetgl1);
				//alert("variabel akhir : " + datetgl1);
				//strtgl1 = addDays(strtgl1, 1);
				//alert("penjualan ke 0 : " + penjualan[0]['tanggal']);
				
			}
			//datetgl1.setDate(datetgl1.getDate() + (-1));
			strtgl1 = ubahkeformatbenar(datetgl1.toString());
				
			console.log("strtgl 1 nilainya adalah : " + strtgl1);

			//---------------------------------------------
			for(var i = 0;i<length;i++)
			{
				if(penjualan[i]['tanggal'] == strtgl1)
				{
					start = true;

				}
				
				if(penjualan[i]['tanggal'] == strtgl2)
				{
					start = false;
					hasilpenjualan.splice(tempi,0,penjualan[i]);
					tempi += 1;
					break;
				}
				if(start == true)
				{
					
					hasilpenjualan.splice(tempi,0,penjualan[i]);
					tempi += 1;
				}

			}
			hasillength = hasilpenjualan.length - 2;
			

			console.log("strtgl1 + strgl2 haha");
				console.log(strtgl1 + " + " + strtgl2);
				console.log("coba penjualan");
				console.log(hasilpenjualan);
				console.log("end of penjualan");
		}
		//membuat tulisan tanggal di bagian paling bawah
		function buattanggaldibawah()
		{
			ctx.fillStyle = "grey";
			ctx.fillText(hasilpenjualan[0]['tanggal'],5,290); 
			ctx.fillStyle = "grey";
			

			ctx.fillText(hasilpenjualan[hasillength - 1]['tanggal'],590,290); 
			
		}

		//mendapatkan penjualanmin dan penjualanmax
		function hariini(){
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();

			if(dd<10) {
			    dd = '0'+dd;
			} 

			if(mm<10) {
			    mm = '0'+mm;
			} 

			today = tambahnoldate(yyyy) + '-' + tambahnoldate(mm) + '-' + tambahnoldate(dd);

			return today;
		}
		function ubahkeformatbenar(x){
			var today = new Date(x);
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();

			if(dd<10) {
			    dd = '0'+dd;
			} 

			if(mm<10) {
			    mm = '0'+mm;
			} 

			today = tambahnoldate(yyyy) + '-' + tambahnoldate(mm) + '-' + tambahnoldate(dd);

			return today;
		}
		//fungsi untuk mengubah yyyy-mm-dd jam-menit-detik ke yyyy-mm-dd
		function ubahkeyyyymmdd(str){
			return str.substring(0,str.indexOf(" "));
		}

		//fungsi untuk memperbaiki 4-5-2017 menjadi 04-05-2017
		function tambahnoldate(x)
		{
			str = x.toString();
			if (str.length  == 1)
			{
				str = "0" + str;
			}
			else
			{
				//alert("str length != 1 -> " + str + " length : " + str.length);
			}
			return str;
		}
		//fungsi untuk menambah date
		function addDays(date, days) {
		  var result = new Date(date);
		  result.setDate(result.getDate() + days);
		  var dd = result.getDate();
		  var mm = result.getMonth() + 1;
		  var y = result.getFullYear();


		  var someFormattedDate = tambahnoldate(y) + '-' + tambahnoldate(mm) + '-' + tambahnoldate(dd);
		  //alert(tambahnoldate(mm));
		  return someFormattedDate;
		}
		//cek koordinat mouse
		function getmousepos(canvas, evt){
			var rect = canvas.getBoundingClientRect();
			return{
			x: evt.clientX - rect.left,
			y: evt.clientY - rect.top
			};
		}
		function buatgarisgrid(){
			//gambar garis grid horizontal

			var tamp = maxtotal - mintotal;
			//console.log(maxtotal + " dan " + mintotal);
			tamp = tamp / 4;
			tamp = Math.round(tamp);

			var txt = parseInt(maxtotal);
			for(var i = 1;i<=5;i++)
			{
				ctx.beginPath();
				ctx.moveTo(0,i * 50);
				ctx.lineTo(600,i*50);
				ctx.strokeStyle = "#e2e2e2";
				ctx.stroke();
				ctx.fillStyle = "black";
				ctx.fillText(txt,630,i * 50); 
				txt -= tamp;
				if(txt <0)
				{
					txt = 0;
				}
			}
		}
		
		function gettitikx(index)
		{
			var tamplength = hasillength;
			var selisih = (600) / (hasillength - 1);
			return (index * selisih)
			//sampai disini
		}
		function buatgarisgrafik(){
			//gambar garis grafik
			//urut dari yang teratas
			ctx.beginPath();

			
			for(var i = 0;i<hasillength;i++)
			{
				//titik[i] = 300 - ((((penjualan[i]["total"] / (maxtotal - mintotal)) + mintotal) * 200) + 50);

				var tamppenjualan = parseInt(hasilpenjualan[i]["total"]);
				
				titik[i] = (tamppenjualan - mintotal) / (maxtotal - mintotal); //ketemu presentase dari penjualan
				titik[i] = 200 - (titik[i] * 200)  + 50; 
				

				if(i != 0)
				{

					ctx.lineTo(gettitikx(i), titik[i]);
					
				}
				else
				{
					ctx.moveTo(0, titik[i]);
				}
				//alert(titik[i]);
			}
			ctx.strokeStyle = "black";
			ctx.stroke();
		}
		
		function buatbulatangrafik(){
			//gambar bulatan
			//diameter 20px
			for(var i = 0;i<hasillength;i++)
			{
				ctx.fillStyle = "black";
				ctx.beginPath();
	    		ctx.arc(gettitikx(i), titik[i], 5, 0, Math.PI * 2, true); // Outer circle
	    		ctx.closePath();
				ctx.fill();
			}
		}
		

		

		
		function hovermouse(evt)
		{
			var mousepos = getmousepos(canvas, evt);
			mx = mousepos.x;
			my = mousepos.y;
		}

		//BUAT ANIAMSI SAAT MOUSE HOVER DI BULATAN, MAKA MUNCUL KETERANGAN
		//lebar : 100
		//tinggi : 50
		function saatdihover(){
			for(var i = 0;i<hasillength;i++)
			{
				

				//jika mouse bertabrakan dengan titik lingkaran
				if(mx >= gettitikx(i)-5  && mx <= gettitikx(i) + 10 )
				{
					if(my >= titik[i] - 10 && my <= titik[i] + 10)
					{
						//jika titik lingkaran berada terlalu dikiri
						var kiri = 0;
						if(mx < 100)
						{
							kiri = 100;
						}
						ctx.fillStyle="black";
						ctx.fillRect(kiri + mx - 100,my,100,50); 

						//membuat tulisan total
						var total = hasilpenjualan[i]['total'];
						//alert('error aneh : ' + hasilpenjualan[i]['total'] + ' -> ' + hasilpenjualan[i]['total']);
						
						total = "Pendapatan : " + total.toString();

						//membuat tulisan tanggal
						var tgl = hasilpenjualan[i]['tanggal'];
						tgl = "Tgl : " + tgl;

						//draw tulisan
						ctx.fillStyle = "white";
						ctx.fillText(total,mx - 95 + kiri,my + 15); 
						ctx.fillStyle = "white";
						ctx.fillText(tgl,mx - 95 + kiri,my + 35); 
					}
				}
			}
		}
		
	</script>
	<?php
		require "tutupkoneksi.php";
	?>
</html>