<?php
	include "koneksi.php";
	if(isset($_GET['id_jabatan'])) //edit sesuai nama tabel
	{
		$id_jabatan = $_GET['id_jabatan'];
		
		//Jika ada variabel tanggal
		
		$query = "DELETE from jabatan where id_jabatan = '$id_jabatan'";

		$result = mysqli_query($kon, $query);
		if($result)
		{
			header('Location: gudeg_duta_jabatan.php'); //diganti sesuai nama tabel
		}
		else
		{
			echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_jabatan.php\">Klik Untuk Kembali</a>";
		}

	}
	else if(isset($_GET['id_absen'])) //edit sesuai nama tabel
	{
		$id_absen = $_GET['id_absen'];
		
		//Jika ada variabel tanggal
		
		$query = "DELETE from absen where id_absen = '$id_absen'";

		$result = mysqli_query($kon, $query);
		if($result)
		{
			header('Location: gudeg_duta_absen.php'); //diganti sesuai nama tabel
		}
		else
		{
			echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_absen.php\">Klik Untuk Kembali</a>";
		}

	}
	else if (isset($_GET['id_diskon'])) {
		$id_diskon = $_GET['id_diskon'];
		
		//Jika ada variabel tanggal
		
		$query = "DELETE from diskon where id_diskon = '$id_diskon'";

		$result = mysqli_query($kon, $query);
		if($result)
		{
			header('Location: gudeg_duta_diskon.php'); //diganti sesuai nama tabel
		}
		else
		{
			echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_diskon.php\">Klik Untuk Kembali</a>";
		}
	}
	else if (isset($_GET['id_karyawan'])) {
		$id_karyawan = $_GET['id_karyawan'];
		
		//Jika ada variabel tanggal
		
		$query = "DELETE from karyawan where id_karyawan = '$id_karyawan'";

		$result = mysqli_query($kon, $query);
		if($result)
		{
			header('Location: gudeg_duta_karyawan.php'); //diganti sesuai nama tabel
		}
		else
		{
			echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_karyawan.php\">Klik Untuk Kembali</a>";
		}
	}
	else if (isset($_GET['id_kategori'])) {
		$id_kategori = $_GET['id_kategori'];
		
		//Jika ada variabel tanggal
		if($_GET['tabel'] == 'kategori_makanan')
		{
			$query = "DELETE from kategori_makanan where id_kategori = '$id_kategori'";

			$result = mysqli_query($kon, $query);
			if($result)
			{
				header('Location: gudeg_duta_kategori_makanan.php'); //diganti sesuai nama tabel
			}
			else
			{
				echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_kategori_makanan.php\">Klik Untuk Kembali</a>";
			}
		}
		else if($_GET['tabel'] == 'kategori_transaksi')
		{
			$query = "DELETE from kategori_transaksi where id_kategori = '$id_kategori'";

			$result = mysqli_query($kon, $query);
			if($result)
			{
				header('Location: gudeg_duta_kategori_transaksi.php'); //diganti sesuai nama tabel
			}
			else
			{
				echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_kategori_transaksi.php\">Klik Untuk Kembali</a>";
			}
		}

		
	}
	else if (isset($_GET['id_transaksi'])) {
		$id_transaksi = $_GET['id_transaksi'];
		
		//Jika ada variabel tanggal
		
		$query = "DELETE from keuangan where id_transaksi = '$id_transaksi'";

		$result = mysqli_query($kon, $query);
		if($result)
		{
			header('Location: gudeg_duta_keuangan.php'); //diganti sesuai nama tabel
		}
		else
		{
			echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_keuangan.php\">Klik Untuk Kembali</a>";
		}
	}
	else if (isset($_GET['id_makanan'])) {
		$id_makanan = $_GET['id_makanan'];
		
		//Jika ada variabel tanggal
		
		$query = "DELETE from makanan where id_makanan = '$id_makanan'";

		$result = mysqli_query($kon, $query);
		if($result)
		{
			header('Location: gudeg_duta_makanan.php'); //diganti sesuai nama tabel
		}
		else
		{
			echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_makanan.php\">Klik Untuk Kembali</a>";
		}
	}
	else if (isset($_GET['id_meja'])) {
		$id_meja= $_GET['id_meja'];
		
		//Jika ada variabel tanggal
		
		$query = "DELETE from meja where id_meja = '$id_meja'";

		$result = mysqli_query($kon, $query);
		if($result)
		{
			header('Location: gudeg_duta_meja.php'); //diganti sesuai nama tabel
		}
		else
		{
			echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_meja.php\">Klik Untuk Kembali</a>";
		}
	}
	else if (isset($_GET['id_penjualan'])) {
		$id_penjualan = $_GET['id_penjualan'];
		
		//Jika ada variabel tanggal
		
		$query = "DELETE from nota_penjualan where id_penjualan = '$id_penjualan'";

		$result = mysqli_query($kon, $query);
		if($result)
		{
			header('Location: gudeg_duta_nota_penjualan.php'); //diganti sesuai nama tabel
		}
		else
		{
			echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_nota_penjualan.php\">Klik Untuk Kembali</a>";
		}
	}
	else if (isset($_GET['id_pelanggan'])) {
		$id_pelanggan = $_GET['id_pelanggan'];
		
		//Jika ada variabel tanggal
		
		$query = "DELETE from pelanggan where id_pelanggan = '$id_pelanggan'";

		$result = mysqli_query($kon, $query);
		if($result)
		{
			header('Location: gudeg_duta_pelanggan.php'); //diganti sesuai nama tabel
		}
		else
		{
			echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_pelanggan.php\">Klik Untuk Kembali</a>";
		}
	}
	else if (isset($_GET['id_presensi'])) {
		$id_presensi = $_GET['id_presensi'];
		
		//Jika ada variabel tanggal
		
		$query = "DELETE from presensi where id_presensi= '$id_presensi'";

		$result = mysqli_query($kon, $query);
		if($result)
		{
			header('Location: gudeg_duta_presensi.php'); //diganti sesuai nama tabel
		}
		else
		{
			echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_presensi.php\">Klik Untuk Kembali</a>";
		}
	}
	else if (isset($_GET['id_reservasi'])) {
		$id_reservasi = $_GET['id_reservasi'];
		
		//Jika ada variabel tanggal
		
		$query = "DELETE from reservasi where id_reservasi = '$id_reservasi'";

		$result = mysqli_query($kon, $query);
		if($result)
		{
			header('Location: gudeg_duta_reservasi.php'); //diganti sesuai nama tabel
		}
		else
		{
			echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_reservasi.php\">Klik Untuk Kembali</a>";
		}
	}
	else if (isset($_GET['id_rincian'])) {
		$id_rincian = $_GET['id_rincian'];
		
		//Jika ada variabel tanggal
		
		$query = "DELETE from rincian where id_rincian = '$id_rincian'";

		$result = mysqli_query($kon, $query);
		if($result)
		{
			header('Location: gudeg_duta_rincian.php'); //diganti sesuai nama tabel
		}
		else
		{
			echo "Tabel yang berisi foreign key harus dihapus / diganti terlebih dahulu <a href=\"gudeg_duta_rincian.php\">Klik Untuk Kembali</a>";
		}
	}
?>