<!DOCTYPE html>
<html>
	<head>
		<title>Edit Pelanggan</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_pelanggan.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_pelanggan.php">Pelanggan</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Pelanggan</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					if(isset($_GET['id_pelanggan']))
					{
						$id_pelanggan = $_GET['id_pelanggan'];
						//mencegah sql injection
						if(is_numeric($id_pelanggan))
						{
							$query = "select * from pelanggan where id_pelanggan=$id_pelanggan";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								
								$nama = $hasil['nama'];
								$no_hp = $hasil['no_hp'];
								$member = $hasil['member'];
								
								
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID pelanggan : </label> <input type=\"text\" name=\"id_pelanggan\" value=\"$id_pelanggan\" readonly/><br>";	
								//membuat input yang bisa diedit2
								//nama
								echo "<label class=\"frm\">Nama : </label> <input type=\"text\" name=\"nama\" value=\"$nama\"/><br>";

								//no hp
								echo "<label class=\"frm\">No. HP : </label> <input type=\"text\" name=\"no_hp\" value=\"$no_hp\"/><br>";

								//member
								echo "<label class=\"frm\">Member : </label>";
								echo "<select name=\"member\">";
								if($member == (YES)){
									echo "<option value=\"YES\" selected=\"selected\">YES</option>";
									echo "<option value=\"NO\" >NO</option>";
								}
								else
								{
									echo "<option value=\"YES\" >YES</option>";
									echo "<option value=\"NO\" selected=\"selected\">NO</option>";
								}
								echo "</select>";

								echo "<input type=\"hidden\" name=\"tabel\" value=\"pelanggan\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>