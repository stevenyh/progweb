<!DOCTYPE html>
<html>
	<head>
		<title>Tambah presensi</title> <!--ganti nama tabel-->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master_guest.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_presensi.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_presensi.php">presensi</a> >  <a href="javascript:window.location.href=window.location.href">Tambah</a>  <!--ganti nama tabel-->
				
			</div>
			<br>
			<div id="isi">
				<h2>Tambah presensi</h2> <!--ganti nama tabel-->
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					//membuat input yang bisa diedit2
					echo "<form action=\"tambah.php\" method=\"post\">";

//id_karyawan
					echo "<label class=\"frm\">ID Karyawan : </label>";
					echo "<select name=\"id_karyawan\">";
					echo "<option disabled selected value> -- select an option -- </option>";
					$query2 = "Select id_karyawan,nama from karyawan";

					$result2 = mysqli_query($kon, $query2);
					$hasil2 = array();
					while($row = mysqli_fetch_assoc($result2))
					{
						$hasil2[] = $row;
					}

					$counter2 = 0;
					foreach($hasil2 as $baris)
					{
														
						$temp1 = $baris['id_karyawan'];
						$temp2 = $baris['nama'];

						
						echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
						
						$counter2 += 1;
														
					}
					echo "</select><br>";
					unset($query2);
					unset($result2);
					unset($hasil2);
					unset($counter2);

//id_absen
					echo "<label class=\"frm\">ID Absen : </label>";
					echo "<select name=\"id_absen\">";
					echo "<option disabled selected value> -- select an option -- </option>";
					$query2 = "Select id_absen,keterangan from absen";

					$result2 = mysqli_query($kon, $query2);
					$hasil2 = array();
					while($row = mysqli_fetch_assoc($result2))
					{
						$hasil2[] = $row;
					}

					$counter2 = 0;
					foreach($hasil2 as $baris)
					{
														
						$temp1 = $baris['id_absen'];
						$temp2 = $baris['keterangan'];

						
						echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
						
						$counter2 += 1;
														
					}
					echo "</select><br>";
					unset($query2);
					unset($result2);
					unset($hasil2);
					unset($counter2);

//jam_masuk
echo "<label class=\"frm\">Jam Masuk : </label>";
echo "<input type=\"datetime-local\" name=\"jam_masuk\" value=\"\"/><br>";


//jam_pulang
echo "<label class=\"frm\">Jam Pulang : </label>";
echo "<input type=\"datetime-local\" name=\"jam_pulang\" value=\"\"/><br>";


//input hidden
					echo "<input type=\"hidden\" name=\"tabel\" value=\"presensi\"><br>"; //ganti value
					echo"<input type=\"submit\" value=\"Tambah\"/>";

					echo "</form>"
						
				?>	
			</div>
			
			
		</div>
	</div>
	
	</body>
	<?php
mysqli_close($kon);
?>
</html>

