<!DOCTYPE html>
<html>
	<head>
		<title>Daftar Reservasi</title> <!-- diganti sesuai nama tabel -->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master_guest.html";
		?>
		<div class="content">
			
			<div id="breadcrumb" style="margin-left:0px; width: 95%">

				<a href="javascript:window.location.href=window.location.href">Reservasi</a>   <!-- diganti -->
				
			</div>
			<br>
			<div id="isi">
				<h2>Daftar Reservasi</h2> <!-- diganti sesuai nama tabel -->
				<a href="tambah_reservasi.php"><span class="btn">Tambah</span></a>
				<table>
				<thead>
					<tr>
						<td>ID Reservasi</td> <!-- diganti sesuai nama kolom tabel -->
						<td>ID Karyawan</td> <!-- diganti sesuai nama kolom tabel -->
						<td>ID Pelanggan</td> <!-- diganti sesuai nama kolom tabel -->
						<td>Waktu Pesan</td> <!-- diganti sesuai nama kolom tabel -->
						<td>Waktu Pakai</td> <!-- diganti sesuai nama kolom tabel -->
						<td>Keterangan</td> <!-- diganti sesuai nama kolom tabel -->
						<td></td>
					</tr>
				</thead>
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					
						
					$query = "select * from reservasi"; //diganti sesuai nama tabel
					$result = mysqli_query($kon, $query);
					$hasil = array();
					while($row = mysqli_fetch_assoc($result))
					{
						$hasil[] = $row;
					}
					$idtabel = -99;
					foreach($hasil  as $baris)
					{
						echo "<tr>";
						$kolomke = 0;
						$id = $baris['id_reservasi'];
						foreach($baris as $kolom)
						{
							if($kolomke == 1) //jika kolom id jabatan
							{
								$query2 = "select username from karyawan where id_karyawan='$kolom'";
								$result2 = mysqli_query($kon, $query2);
								$row2 = mysqli_fetch_assoc($result2);
								echo "<td>" . $kolom . ' - ' . $row2['username'] . "</td>";
							}
							else if($kolomke == 2) //jika kolom id jabatan
							{
								$query2 = "select nama from pelanggan where id_pelanggan='$kolom'";
								$result2 = mysqli_query($kon, $query2);
								$row2 = mysqli_fetch_assoc($result2);
								echo "<td>". $kolom .' - ' . $row2['nama'] . "</td>";
							}
							else
							{
								echo "<td>" . $kolom . "</td>";
							}
							if($idtabel == -99)
							{
								$idtabel = $kolom;
							}
							$kolomke += 1;
						}
						echo "<td><a href=\"edit_reservasi.php?id_reservasi=$id\"><img src=\"edit.png\" width=20 height=20 /></a><a href=\"hapus.php?id_reservasi=$id\" class=\"ahapus\"><img src=\"iDelete.png\" width=20 height=20/></a></td>"; 
						//edit_presensi.php DIGANTI NAMA edit_(tabel).php?(NAMA KOLOM PK)=$idtabel
						
						echo "</tr>";
					}	
					

					
				?>	
			</div>
			
			
		</div>
	</div>
	
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>