<!DOCTYPE html>
<html>
	<head>
		<title>Tambah nota_penjualan</title> <!--ganti nama tabel-->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<script src="jquery-3.3.1.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master_guest.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_nota_penjualan.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_nota_penjualan.php">nota_penjualan</a> >  <a href="javascript:window.location.href=window.location.href">Tambah</a>  <!--ganti nama tabel-->
				
			</div>
			<br>
			<div id="isi">
				<h2>Tambah nota_penjualan</h2> <!--ganti nama tabel-->
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					//membuat input yang bisa diedit2
					echo "<form action=\"tambah.php\" method=\"post\" onsubmit=\"selectAll('pilihmakanan',true)\">";
					//id_karyawan
					echo "<label class=\"frm\">ID Karyawan : </label>";
					echo "<select name=\"id_karyawan\">";
					echo "<option disabled selected value> -- select an option -- </option>";
					$query2 = "Select id_karyawan,nama from karyawan";

					$result2 = mysqli_query($kon, $query2);
					$hasil2 = array();
					while($row = mysqli_fetch_assoc($result2))
					{
						$hasil2[] = $row;
					}

					$counter2 = 0;
					foreach($hasil2 as $baris)
					{
														
						$temp1 = $baris['id_karyawan'];
						$temp2 = $baris['nama'];

						
						echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
						
						$counter2 += 1;
														
					}
					echo "</select><br>";
					unset($query2);
					unset($result2);
					unset($hasil2);
					unset($counter2);

//id_pelanggan
					echo "<label class=\"frm\">ID Pelanggan : </label>";
					echo "<select name=\"id_pelanggan\">";
					echo "<option disabled selected value> -- select an option -- </option>";
					$query2 = "Select id_pelanggan,nama from pelanggan";

					$result2 = mysqli_query($kon, $query2);
					$hasil2 = array();
					while($row = mysqli_fetch_assoc($result2))
					{
						$hasil2[] = $row;
					}

					$counter2 = 0;
					foreach($hasil2 as $baris)
					{
														
						$temp1 = $baris['id_pelanggan'];
						$temp2 = $baris['nama'];

						
						echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
						
						$counter2 += 1;
														
					}
					echo "</select><br>";
					unset($query2);
					unset($result2);
					unset($hasil2);
					unset($counter2);

//id_meja
					echo "<label class=\"frm\">ID Meja : </label>";
					echo "<select name=\"id_meja\">";
					echo "<option disabled selected value> -- select an option -- </option>";
					$query2 = "Select id_meja,no_meja from meja";

					$result2 = mysqli_query($kon, $query2);
					$hasil2 = array();
					while($row = mysqli_fetch_assoc($result2))
					{
						$hasil2[] = $row;
					}

					$counter2 = 0;
					foreach($hasil2 as $baris)
					{
														
						$temp1 = $baris['id_meja'];
						$temp2 = $baris['no_meja'];

						
						echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
						
						$counter2 += 1;
														
					}
					echo "</select><br>";
					unset($query2);
					unset($result2);
					unset($hasil2);
					unset($counter2);

//id_reservasi
					echo "<label class=\"frm\">ID Reservasi : </label>";
					echo "<select name=\"id_reservasi\">";
					echo "<option disabled selected value> -- select an option -- </option>";
					$query2 = "Select id_reservasi,waktu_pesan from reservasi";

					$result2 = mysqli_query($kon, $query2);
					$hasil2 = array();
					while($row = mysqli_fetch_assoc($result2))
					{
						$hasil2[] = $row;
					}

					$counter2 = 0;
					foreach($hasil2 as $baris)
					{
														
						$temp1 = $baris['id_reservasi'];
						$temp2 = $baris['waktu_pesan'];

						
						echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
						
						$counter2 += 1;
														
					}
					echo "</select><br>";
					unset($query2);
					unset($result2);
					unset($hasil2);
					unset($counter2);

//id_diskon
					echo "<label class=\"frm\">ID Diskon : </label>";
					echo "<select name=\"id_diskon\">";
					echo "<option disabled selected value> -- select an option -- </option>";
					$query2 = "Select id_diskon,diskon from diskon";

					$result2 = mysqli_query($kon, $query2);
					$hasil2 = array();
					while($row = mysqli_fetch_assoc($result2))
					{
						$hasil2[] = $row;
					}

					$counter2 = 0;
					foreach($hasil2 as $baris)
					{
														
						$temp1 = $baris['id_diskon'];
						$temp2 = $baris['diskon'];

						
						echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
						
						$counter2 += 1;
														
					}
					echo "</select><br>";
					unset($query2);
					unset($result2);
					unset($hasil2);
					unset($counter2);

//tanggal
echo "<label class=\"frm\">Tanggal : </label>";
echo "<input type=\"datetime-local\" name=\"Tanggal\" value=\"\"/><br>";


//total
					echo "<label class=\"frm\">Total : </label> <input type=\"number\" name=\"total\" value=\"\"/><br>";

//input hidden
					echo "<input type=\"hidden\" name=\"tabel\" value=\"nota_penjualan\"><br>"; //ganti value
					
						
				?>	
				<!-- INI HANYA TAMPILAN -->
					<br><label><b> Makanan yang dipesan : </b></label><br>
					
					<!-- membuat select makanan dengan PHP -->
					<?php 
						echo "<select id=\"listmakanan\" name=\"id_makanan\"  >";
						echo "<option disabled selected value> -- select an option -- </option>";
						$query2 = "Select id_makanan,nama from makanan";

						$result2 = mysqli_query($kon, $query2);
						$hasil2 = array();
						while($row = mysqli_fetch_assoc($result2))
						{
							$hasil2[] = $row;
						}

						$counter2 = 0;
						foreach($hasil2 as $baris)
						{
															
							$temp1 = $baris['id_makanan'];
							$temp2 = $baris['nama'];

							
							echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
							
							$counter2 += 1;
															
						}
						echo "</select>";
						unset($query2);
						unset($result2);
						unset($hasil2);
						unset($counter2);
					 ?>	
					<input  type="button" id="btntambahmakanan" style="height:36px;" value="Tambahkan ke List">	
					<br>
					<select id="pilihmakanan" name="sometext[]" size="10" multiple="multiple">
						  
					</select>		
					
					<div id="keterangantambah">
						<label id="keterangantambah_makanan">Makanan : -</label><br>
						<label id="keterangantambah_porsi">Porsi : 0</label><input type="button" id="btntambahporsi" value="+"><input type="button" id="btnkurangporsi" value="-"/>
					</div>
				<!-- END -->
				<div style="clear:left"></div>
				<br>
				<?php 
					echo "<input type=\"submit\" value=\"Tambah\"/>";
					echo "</form>";
				 ?>
			</div>
			
			
		</div>
	</div>
	<script>
		function dapatkanid(str)
		{
			var tamp = str;
			tamp = tamp.substr(0,tamp.indexOf(' -'));
			return tamp;
		}
		function dapatkantext(str)
		{
			var tamp = str;
			tamp = tamp.substr(tamp.indexOf(' - ') + 3);
			return tamp;
		}
		function dapatkanporsi(str)
		{
			var tamp = str;
			tamp = tamp.substr(tamp.indexOf('(') + 1);
			tamp = tamp.substr(0, tamp.indexOf(')'));
			return tamp;
		}
		function naikanporsi_value(str, angka)
		{
			var id = str.substr(0, str.indexOf(' - ') + 3);
			var porsi = str.substr(str.indexOf(' - ') + 3);
			var intporsi = parseInt(porsi);
			intporsi += angka;
			var hasil = id + intporsi.toString();
			return hasil;
		}
		function naikanporsi_text(str, angka)
		{
			var id = str.substr(0, str.indexOf(' - ') + 3);
			var nama = str.substr(str.indexOf(' - ') + 3);
			var porsi = nama.substr(nama.indexOf('(') + 1);
			porsi = porsi.substr(0, porsi.indexOf(')'));
			nama = nama.substr(0, nama.indexOf('('));
			var intporsi = parseInt(porsi);
			intporsi += angka;
			var hasil = id + nama + '(' + intporsi.toString() + ')';
			return hasil;
		}
		$('#btntambahmakanan').click(function(){

			var strmakananterpilih = $('#listmakanan option:selected').text();
			if(strmakananterpilih != "-- select an option --")
			{
				var strmakananterpilih_id = dapatkanid(strmakananterpilih);
				var strmakananterpilih_text = dapatkantext(strmakananterpilih);
				//strmakananterpilih = 1 - bakmi
				//strmakananterpilih_id = 1
				//strmakananterpilih_text= bakmi
				
				//pengecekan apakah makanan tersebut sudah ada di list atau belum
				var opsi = document.getElementById('pilihmakanan').options;
				var ketemu = -1;
				for(var i = 0;i<opsi.length;i++)
				{
					if(strmakananterpilih_id == dapatkanid(opsi[i].value))
					{
						ketemu = i;
						break;
					}
				}
				//alert(ketemu);
				if(ketemu == -1)
				{

					$('#pilihmakanan').append("<option value='" + strmakananterpilih_id + " - 1'>" + strmakananterpilih_id + " - " + strmakananterpilih_text + "(1)</option>");
				}
				else
				{
					//alert("mz");
					//alert(naikanporsi_value(opsi[ketemu].value));
					//alert(naikanporsi_text(opsi[ketemu].text));
					opsi[ketemu].value = naikanporsi_value(opsi[ketemu].value,1);
					opsi[ketemu].text = naikanporsi_text(opsi[ketemu].text,1);
				}
			}
			
		})

		$('#pilihmakanan').change(function(){

			var ini = $("#pilihmakanan option:selected").text();
			var tamp = dapatkantext(ini);
			tamp = tamp.substr(0, tamp.indexOf('('));
			$('#keterangantambah_makanan').html('Makanan : ' + tamp)
			$('#keterangantambah_porsi').html('Porsi : ' + dapatkanporsi(ini))
		})

		$('#btntambahporsi').click(function(){
			var opsi = document.getElementById('pilihmakanan').options;
			var x = document.getElementById("pilihmakanan").selectedIndex;	
			if(x != -1)
			{
				opsi[x].value = naikanporsi_value(opsi[x].value,1);
				opsi[x].text = naikanporsi_text(opsi[x].text,1);
				var tamp = $('#keterangantambah_porsi').html();
				tamp = "Porsi : " + (parseInt(tamp.substr(tamp.indexOf(" : ") + 3)) + 1).toString();
				$('#keterangantambah_porsi').html(tamp);
			}
		})

		$('#btnkurangporsi').click(function(){
			var opsi = document.getElementById('pilihmakanan').options;
			var x = document.getElementById("pilihmakanan").selectedIndex;	

			if(x != -1)
			{
				var porsisekarang = opsi[x].text;
				if (dapatkanporsi(porsisekarang) > 0)
				{
					opsi[x].value = naikanporsi_value(opsi[x].value,-1);
					opsi[x].text = naikanporsi_text(opsi[x].text,-1);
					var tamp = $('#keterangantambah_porsi').html();
					tamp = "Porsi : " + (parseInt(tamp.substr(tamp.indexOf(" : ") + 3)) - 1).toString();
					$('#keterangantambah_porsi').html(tamp);
					
				}
				else
				{
					alert("porsi harus lebih besar dari 0");
				}
			}
		})
		function selectAll(selectBox,selectAll) { 
	    	//alert(selectBox);
	        var selectBox2 = document.getElementById('pilihmakanan').options;
	    
	    // is the select box a multiple select box? 
	    
	        for (var i = 0; i < selectBox2.length; i++) { 
	             selectBox2[i].selected = selectAll; 
	        } 
	    
	    }
	</script>
	</body>
</html>

