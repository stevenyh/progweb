<?php
	session_start();
	if(!isset($_SESSION["user"])) {
		$message = "Anda harus login terlebih dahulu";
		echo "<script type='text/javascript'>alert('$message');</script>";
		header("Location: login.php");
	}
?>