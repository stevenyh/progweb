<!DOCTYPE html>
<html>
	<head>
		<title>Edit Presensi</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master_guest.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_presensi.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_presensi.php">Presensi</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Presensi</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					if(isset($_GET['id_presensi']))
					{
						$id_presensi = $_GET['id_presensi'];
						//mencegah sql injection
						if(is_numeric($id_presensi))
						{
							$query = "select * from presensi where id_presensi=$id_presensi";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								
								$id_karyawan = $hasil['id_karyawan'];
								$id_absen = $hasil['id_absen'];
								$jam_masuk = $hasil['jam_masuk'];
								$jam_pulang = $hasil['jam_pulang'];
								
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID Presensi : </label> <input type=\"text\" name=\"id_presensi\" value=\"$id_presensi\" readonly/><br>";	
								//membuat input yang bisa diedit2
								//id karyawan
								echo "<label class=\"frm\">ID Karyawan : </label>";
								echo "<select name=\"id_karyawan\">";
								echo "<option disabled selected value> -- select an option -- </option>";
								$query2 = "Select id_karyawan,username from karyawan";

								$result2 = mysqli_query($kon, $query2);
								$hasil2 = array();
								while($row = mysqli_fetch_assoc($result2))
								{
									$hasil2[] = $row;
								}

								$counter2 = 0;
								foreach($hasil2 as $baris)
								{
																	
									$temp1 = $baris['id_karyawan'];
									$temp2 = $baris['username'];

									if($id_karyawan == $temp1)
									{

										echo "<option value=\"$temp1\" selected=\"selected\">$temp1 - $temp2</option>";
									}
									else
									{
										echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
									}
									$counter2 += 1;
																	
								}
								echo "</select><br>";
								unset($query2);
								unset($result2);
								unset($hasil2);
								unset($counter2);

								//id absen
								echo "<label class=\"frm\">ID Absen : </label>";
								echo "<select name=\"id_absen\">";
								echo "<option disabled selected value> -- select an option -- </option>";
								$query2 = "Select id_absen,keterangan from absen";

								$result2 = mysqli_query($kon, $query2);
								$hasil2 = array();
								while($row = mysqli_fetch_assoc($result2))
								{
									$hasil2[] = $row;
								}

								$counter2 = 0;
								foreach($hasil2 as $baris)
								{
																	
									$temp1 = $baris['id_absen'];
									$temp2 = $baris['keterangan'];

									if($id_absen == $temp1)
									{

										echo "<option value=\"$temp1\" selected=\"selected\">$temp1 - $temp2</option>";
									}
									else
									{
										echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
									}
									$counter2 += 1;
																	
								}
								echo "</select><br>";
								unset($query2);
								unset($result2);
								unset($hasil2);
								unset($counter2);

								//jam masuk
								echo "<label class=\"frm\">Jam Masuk : </label>";
								$newdate = date('Y-m-d\TH:i', strtotime($jam_masuk));
								echo "<input type=\"datetime-local\" name=\"jam_masuk\" value=\"$newdate\"/><br>";
								unset($newdate);

								//jam pulang
								echo "<label class=\"frm\">Jam Pulang : </label>";
								$newdate = date('Y-m-d\TH:i', strtotime($jam_pulang));
								echo "<input type=\"datetime-local\" name=\"jam_pulang\" value=\"$newdate\"/><br>";
								unset($newdate);

								echo "<input type=\"hidden\" name=\"tabel\" value=\"presensi\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>