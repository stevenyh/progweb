<!DOCTYPE html>
<html>
	<head>
		<title>Edit Jabatan</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_jabatan.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_jabatan.php">Jabatan</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Jabatan</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					if(isset($_GET['id_jabatan']))
					{
						$id_jabatan = $_GET['id_jabatan'];
						//mencegah sql injection
						if(is_numeric($id_jabatan))
						{
							$query = "select * from jabatan where id_jabatan=$id_jabatan";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								
								$nama_jabatan = $hasil['nama_jabatan'];
								
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID Jabatan : </label> <input type=\"text\" name=\"id_jabatan\" value=\"$id_jabatan\" readonly/><br>";	
								//membuat input yang bisa diedit2
								//nama jabatan
								echo "<label class=\"frm\">Nama Jabatan : </label> <input type=\"text\" name=\"nama_jabatan\" value=\"$nama_jabatan\"/><br>";

								echo "<input type=\"hidden\" name=\"tabel\" value=\"jabatan\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>