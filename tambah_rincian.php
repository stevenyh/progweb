<!DOCTYPE html>
<html>
	<head>
		<title>Tambah rincian</title> <!--ganti nama tabel-->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master_guest.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_rincian.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_rincian.php">rincian</a> >  <a href="javascript:window.location.href=window.location.href">Tambah</a>  <!--ganti nama tabel-->
				
			</div>
			<br>
			<div id="isi">
				<h2>Tambah rincian</h2> <!--ganti nama tabel-->
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					//membuat input yang bisa diedit2
					echo "<form action=\"tambah.php\" method=\"post\">";

//id_penjualan
					echo "<label class=\"frm\">ID Penjualan : </label>";
					echo "<select name=\"id_penjualan\">";
					echo "<option disabled selected value> -- select an option -- </option>";
					$query2 = "Select id_penjualan,total from nota_penjualan";

					$result2 = mysqli_query($kon, $query2);
					$hasil2 = array();
					while($row = mysqli_fetch_assoc($result2))
					{
						$hasil2[] = $row;
					}

					$counter2 = 0;
					foreach($hasil2 as $baris)
					{
														
						$temp1 = $baris['id_penjualan'];
						$temp2 = $baris['total'];

						
						echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
						
						$counter2 += 1;
														
					}
					echo "</select><br>";
					unset($query2);
					unset($result2);
					unset($hasil2);
					unset($counter2);

//id_makanan
					echo "<label class=\"frm\">ID Makanan : </label>";
					echo "<select name=\"id_makanan\">";
					echo "<option disabled selected value> -- select an option -- </option>";
					$query2 = "Select id_makanan,nama from makanan";

					$result2 = mysqli_query($kon, $query2);
					$hasil2 = array();
					while($row = mysqli_fetch_assoc($result2))
					{
						$hasil2[] = $row;
					}

					$counter2 = 0;
					foreach($hasil2 as $baris)
					{
														
						$temp1 = $baris['id_makanan'];
						$temp2 = $baris['nama'];

						
						echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
						
						$counter2 += 1;
														
					}
					echo "</select><br>";
					unset($query2);
					unset($result2);
					unset($hasil2);
					unset($counter2);

//kuantitas
					echo "<label class=\"frm\">Kuantitas : </label> <input type=\"number\" name=\"kuantitas\" value=\"\"/><br>";

//input hidden
					echo "<input type=\"hidden\" name=\"tabel\" value=\"rincian\"><br>"; //ganti value
					echo"<input type=\"submit\" value=\"Tambah\"/>";

					echo "</form>"
						
				?>	
			</div>
			
			
		</div>
	</div>
	
	</body>
	<?php
mysqli_close($kon);
?>
</html>

