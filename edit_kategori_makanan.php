<!DOCTYPE html>
<html>
	<head>
		<title>Edit Kategori Makanan</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_kategori_makanan.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_kategori_makanan.php">Kategori Makanan</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Kategori Makanan</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					if(isset($_GET['id_kategori']))
					{
						$id_kategori = $_GET['id_kategori'];
						//mencegah sql injection
						if(is_numeric($id_kategori))
						{
							$query = "select * from kategori_makanan where id_kategori=$id_kategori";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								$keterangan = $hasil['keterangan'];
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID Kategori : </label> <input type=\"text\" name=\"ID Kategori\" value=\"$id_kategori\" readonly/><br>";	
								//membuat input yang bisa diedit2
								//keterangan
								echo "<label class=\"frm\">Keterangan : </label> <input type=\"text\" name=\"keterangan\" value=\"$keterangan\"/><br>";
								
								echo "<input type=\"hidden\" name=\"tabel\" value=\"kategori_makanan\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>