<!DOCTYPE html>
<html>
	<head>
		<title>Daftar Absen</title> <!-- diganti sesuai nama tabel -->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master_guest.html";
		?>
		<div class="content">
			
			<div id="breadcrumb" style="margin-left:0px; width: 95%">

				<a href="javascript:window.location.href=window.location.href">Absen</a>   <!-- diganti -->
				
			</div>
			<br>
			<div id="isi">
				<h2>Daftar Absen</h2> <!-- diganti sesuai nama tabel -->
				<a href="tambah_absen.php"><span class="btn">Tambah</span></a>
				<table>
				<thead>
					<tr>
						<td>ID Absen</td> <!-- diganti sesuai nama kolom tabel -->
						<td>Keterangan</td> <!-- diganti sesuai nama kolom tabel -->
						<td></td>
					</tr>
				</thead>
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					
						
					$query = "select * from absen"; //diganti sesuai nama tabel
					$result = mysqli_query($kon, $query);
					$hasil = array();
					while($row = mysqli_fetch_assoc($result))
					{
						$hasil[] = $row;
					}
					$idtabel = -99;
					foreach($hasil  as $baris)
					{
						echo "<tr>";
						$id = $baris['id_absen'];
						foreach($baris as $kolom)
						{
							echo "<td>" . $kolom . "</td>";
							if($idtabel == -99)
							{
								$idtabel = $kolom;
							}
						}
						echo "<td><a href=\"edit_absen.php?id_absen=$id\"><img src=\"edit.png\" width=20 height=20 /></a><a class=\"ahapus\" href=\"hapus.php?id_absen=$id\"><img src=\"iDelete.png\" width=20 height=20/></a></td>"; 
						//edit_presensi.php DIGANTI NAMA edit_(tabel).php?(NAMA KOLOM PK)=$idtabel
						
						echo "</tr>";
					}	
					

					
				?>	
			</div>
			
			
		</div>
	</div>
	
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>