<!DOCTYPE html>
<html>
	<head>
		<title>Tambah Makanan</title> <!--ganti nama tabel-->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_makanan.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_makanan.php">Makanan</a> >  <a href="javascript:window.location.href=window.location.href">Tambah</a>  <!--ganti nama tabel-->
				
			</div>
			<br>
			<div id="isi">
				<h2>Tambah Makanan</h2> <!--ganti nama tabel-->
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					//membuat input yang bisa diedit2
					echo "<form action=\"tambah.php\" method=\"post\">";
					
					//id_kategori
					echo "<label class=\"frm\">ID Kategori : </label>";
					echo "<select name=\"id_kategori\">";
					echo "<option disabled selected value> -- select an option -- </option>";
					$query2 = "Select id_kategori,keterangan from kategori_makanan";

					$result2 = mysqli_query($kon, $query2);
					$hasil2 = array();
					while($row = mysqli_fetch_assoc($result2))
					{
						$hasil2[] = $row;
					}

					$counter2 = 0;
					foreach($hasil2 as $baris)
					{
														
						$temp1 = $baris['id_kategori'];
						$temp2 = $baris['keterangan'];

						if($id_kategori == $temp1)
						{

							echo "<option value=\"$counter2\" selected=\"selected\">$temp1 - $temp2</option>";
						}
						else
						{
							echo "<option value=\"$counter2\" >$temp1 - $temp2</option>";
						}
						$counter2 += 1;
														
					}
					echo "</select><br>";
					unset($query2);
					unset($result2);
					unset($hasil2);
					unset($counter2);

					//nama
					echo "<label class=\"frm\">Nama : </label> <input type=\"text\" name=\"nama\" value=\"\"/><br>";

					//harga
					echo "<label class=\"frm\">Harga : </label> <input type=\"text\" name=\"harga\" value=\"\"/><br>";
					
					//input hidden
					echo "<input type=\"hidden\" name=\"tabel\" value=\"makanan\"><br>"; //ganti value
					echo"<input type=\"submit\" value=\"Tambah\"/>";

					echo "</form>"
						
				?>	
			</div>
			
			
		</div>
	</div>
	
	</body>
	<?php
mysqli_close($kon);
?>
</html>