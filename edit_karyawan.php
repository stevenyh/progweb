<!DOCTYPE html>
<html>
	<head>
		<title>Edit Karyawan</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_karyawan.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_karyawan">Karyawan</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Karyawan</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					if(isset($_GET['id_karyawan']))
					{
						$id_karyawan = $_GET['id_karyawan'];
						//mencegah sql injection
						if(is_numeric($id_karyawan))
						{
							$query = "select * from karyawan where id_karyawan=$id_karyawan";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								$id_jabatan = $hasil['id_jabatan'];
								$username = $hasil['username'];
								$password = $hasil['password'];
								$nama = $hasil['nama'];
								$no_hp = $hasil['no_hp'];
								$email = $hasil['email'];
								$gaji = $hasil['gaji'];
								
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID Karyawan : </label> <input type=\"text\" name=\"id_karyawan\" value=\"$id_karyawan\" readonly/><br>";	
								//membuat input yang bisa diedit2
								//id_jabatan
								echo "<label class=\"frm\">ID Jabatan : </label>";
								echo "<select name=\"id_jabatan\">";
								echo "<option disabled selected value> -- select an option -- </option>";
								$query2 = "Select id_jabatan,nama_jabatan from jabatan";

								$result2 = mysqli_query($kon, $query2);
								$hasil2 = array();
								while($row = mysqli_fetch_assoc($result2))
								{
									$hasil2[] = $row;
								}

								$counter2 = 0;
								foreach($hasil2 as $baris)
								{
																	
									$temp1 = $baris['id_jabatan'];
									$temp2 = $baris['nama_jabatan'];

									if($id_jabatan == $temp1)
									{

										echo "<option value=\"$temp1\" selected=\"selected\">$temp1 - $temp2</option>";
									}
									else
									{
										echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
									}
									$counter2 += 1;
								}
								echo "</select><br>";
								unset($query2);
								unset($result2);
								unset($hasil2);
								unset($counter2);

								//username
								echo "<label class=\"frm\">Username : </label> <input type=\"text\" name=\"username\" value=\"$username\"/><br>";
								
								//password
								echo "<label class=\"frm\">Password : </label> <input type=\"text\" name=\"password\" value=\"$password\"/><br>";

								//nama
								echo "<label class=\"frm\">Nama : </label> <input type=\"text\" name=\"nama\" value=\"$nama\"/><br>";

								//no_hp
								echo "<label class=\"frm\">No. HP : </label> <input type=\"text\" name=\"no_hp\" value=\"$no_hp\"/><br>";

								//email
								echo "<label class=\"frm\">Email : </label> <input type=\"text\" name=\"email\" value=\"$email\"/><br>";

								//gaji
								echo "<label class=\"frm\">Gaji : </label> <input type=\"number\" name=\"gaji\" value=\"$gaji\"/><br>";

								echo "<input type=\"hidden\" name=\"tabel\" value=\"karyawan\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>