<!DOCTYPE html>
<html>
	<head>
		<title>Daftar Akun Karyawan</title> <!-- diganti sesuai nama tabel -->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master.html";
		?>
		<div class="content">
			
			<div id="breadcrumb" style="margin-left:0px; width: 95%">

				<a href="javascript:window.location.href=window.location.href">karyawan</a>   <!-- diganti -->
				
			</div>
			<br>
			<div id="isi">
				<h2>Daftar Akun Karyawan</h2> <!-- diganti sesuai nama tabel -->
				<a href="tambah_karyawan.php"><span class="btn">Tambah</span></a>
				<table>
				<thead>
					<tr>
						<td>ID Karyawan</td> <!-- diganti sesuai nama kolom tabel -->
						<td>ID Jabatan</td> <!-- diganti sesuai nama kolom tabel -->
						<td>Username</td> <!-- diganti sesuai nama kolom tabel -->
						<td style="max-width: 50px;overflow-x: auto">Password</td> <!-- diganti sesuai nama kolom tabel -->
						<td>Nama</td> <!-- diganti sesuai nama kolom tabel -->
						<td>No. Hp</td> <!-- diganti sesuai nama kolom tabel -->
						<td>Email</td> <!-- diganti sesuai nama kolom tabel -->
						<td>Gaji</td> <!-- diganti sesuai nama kolom tabel -->
						<td></td>
					</tr>
				</thead>
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					
						
					$query = "select * from karyawan"; //diganti sesuai nama tabel
					$result = mysqli_query($kon, $query);
					$hasil = array();
					while($row = mysqli_fetch_assoc($result))
					{
						$hasil[] = $row;
					}
					$idtabel = -99;
					foreach($hasil  as $baris)
					{
						echo "<tr>";
						$kolomke = 0;
						$id = $baris['id_karyawan'];
						foreach($baris as $kolom)
						{
							if($kolomke == 1) //jika kolom id jabatan
							{
								$query2 = "select nama_jabatan from jabatan where id_jabatan='$kolom'";
								$result2 = mysqli_query($kon, $query2);
								$row2 = mysqli_fetch_assoc($result2);
								echo "<td>" . $kolom . ' - ' . $row2['nama_jabatan'] . "</td>";
							}
							else if($kolomke == 3)
							{
								echo "<td style=\"max-width:50px;overflow-x:auto\">" . $kolom . "</td>";
							}
							else
							{
								echo "<td>" . $kolom . "</td>";
							}
							if($idtabel == -99)
							{
								$idtabel = $kolom;
							}
							$kolomke += 1;
						}
						echo "<td><a href=\"edit_karyawan.php?id_karyawan=$id\"><img src=\"edit.png\" width=20 height=20 /></a><a href=\"hapus.php?id_karyawan=$id\" class=\"ahapus\"><img src=\"iDelete.png\" width=20 height=20/></a></td>"; 
						//edit_presensi.php DIGANTI NAMA edit_(tabel).php?(NAMA KOLOM PK)=$idtabel
						
						echo "</tr>";
					}	
					

					
				?>	
			</div>
			
			
		</div>
	</div>
	
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>