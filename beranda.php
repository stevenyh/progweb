<!DOCTYPE html>
<html>
	<head>
		<title>Beranda</title> <!--ganti nama tabel-->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script src="jquery-3.3.1.min.js"></script>
	</head>
	<body>
		<?php
			require "master_guest.html";
			require "koneksi.php";
			$query = "select date_format(tanggal,\"%Y-%m-%d\") as tanggal, sum(total) as total from nota_penjualan where cast(tanggal as datetime) > (DATE_ADD(NOW(), interval -7 day)) AND cast(tanggal as datetime) <= NOW() GROUP BY CAST(tanggal AS DATE) order by cast(tanggal as datetime) asc LIMIT 7;";
			$result  = mysqli_query($kon, $query);
			if(!$result)
			{
				echo mysqli_error($kon);
			}
			$hasil = array();
			$length = 0;
			$idx = 0;
			while($row = mysqli_fetch_assoc($result))
			{
				$length += 1;
				$hasil[] = $row;
			}
			echo $length;
			print_r($hasil);
			
		?>
		<div class="content">
			
			<br>
			<div id="isi">
				<center><h2 >BERANDA</h2></center> 

				
				
				<h2 id="judul2"> Reservasi Terbaru </h2>
				<table>
					<thead>
						<tr>
						<td>ID Reservasi</td>
						<td>ID Karyawan</td>
						<td>ID Pelanggan</td>
						<td>Waktu Pesan</td>
						<td>Waktu Pakai</td>
						<td>Keterangan</td>
						</tr>
					</thead>
					<?php 
						$query2 = "select * from reservasi where waktupakai >= now()"; //diganti sesuai nama tabel
						$result2 = mysqli_query($kon, $query2);
						$hasil2 = array();
						while($row = mysqli_fetch_assoc($result2))
						{
							$hasil2[] = $row;
						}
						$idtabel = -99;
						foreach($hasil2  as $baris)
						{
							echo "<tr>";
							foreach($baris as $kolom)
							{
								echo "<td>" . $kolom . "</td>";
								if($idtabel == -99)
								{
									$idtabel = $kolom;
								}
							}
							
							//edit_presensi.php DIGANTI NAMA edit_(tabel).php?(NAMA KOLOM PK)=$idtabel
							
							echo "</tr>";
						}	
					 ?>
				</table>

				<br>
				<br>
				<h2 style="display: inline-block;" id="judul2"> Statistik Penjualan </h2>
				<a href="#" style="margin-left: 350px;display: inline-block">Selengkapnya</a>
				 <br>
				<canvas id="canvas" style="background-color: white; border:1px solid black" width="680" height="300">

				</canvas>
				
			</div>
			
			
		</div>
	</div>
	
	</body>
	<script>
		//membuat variabel untuk canvas
		var canvas = document.getElementById('canvas');
		var ctx = canvas.getContext('2d');

		//membuat variabel untuk menampung hasil query
		var penjualan = [[],[]];
		var length = <?php echo $length;?>;


		penjualan = <?php echo json_encode($hasil, JSON_PRETTY_PRINT) ?>;
		
		//perbaikan array penjualan sesuai format hari ini sampai dengan hari ini dikurangi 7 hari
		var tglawal = addDays(hariini(), -7);
		for(var i = 0;i<7;i++)
		{
			if(!(penjualan[i]))
			{
				var isikosong = [];
				isikosong['tanggal'] = tglawal;
				isikosong['total'] = 0;
				//console.log((penjualan[i]['tanggal']) + " : " + tglawal);
				length += 1;
				penjualan.splice(i,0,isikosong);
			}
			else
			{
				if((penjualan[i]['tanggal']) != tglawal )
				{
					var isikosong = [];
					isikosong['tanggal'] = tglawal;
					isikosong['total'] = 0;
					//console.log((penjualan[i]['tanggal']) + " : " + tglawal);
					length += 1;
					penjualan.splice(i,0,isikosong);
				}
				
			}

			tglawal = addDays(tglawal, 1);
		}
		console.log("penju");
		console.log(penjualan);
		while(penjualan[length - 1]['tanggal'] != hariini())
		{
			var isikosong = [];
			isikosong['tanggal'] = addDays(penjualan[length - 1]['tanggal'], 1);
			isikosong['total'] = 0;
			//console.log((penjualan[i]['tanggal']) + " : " + tglawal);
			length += 1;
			penjualan.splice(length,0,isikosong);
		}
		
		console.log(penjualan);
		//pembuatan variabel maxtotal dan mintotal
		var maxtotal = 0;
		var mintotal = 99999999;

		for(var i = 0;i<length;i++)
		{
			
			
			var tamp = parseInt(penjualan[i]["total"]);
			
			console.log("tamp : " + tamp);
			if(tamp > maxtotal)
			{
				maxtotal = tamp;
			}
			if(tamp < mintotal)
			{
				mintotal = tamp;
			}

			
		}

		//membuat variabel titik yang digunakan sebagai titik penjualan pada canvas
		var titik = [];

		


		//membuat eventlistener untuk mouse
		//mx dan my adalah hasil posisi mouse
		canvas.addEventListener('mousemove',hovermouse , false);
		mx = 0;
		my = 0;

		//jalankan animasi
		setInterval(grafik, 500/15);

		//titik x : (0-600) + 20
		//0 + 20 = 20
		//100 + 20 = 120
		//200 + 20 = 220
		//300 + 20 = 320
		//400 + 20 = 420
		//500 + 20 = 520
		//600 + 20 = 620

		//titik y : (50-250)
		//50
		//100
		//150
		//200
		//250

		

		function grafik(){
			ctx.clearRect(0,0,canvas.width,canvas.height);
			buatgarisgrid();
			buatgarisgrafik();
			buatbulatangrafik();
			buattanggaldibawah();
			saatdihover();
			
		}

		//membuat tulisan tanggal di bagian paling bawah
		function buattanggaldibawah()
		{
			ctx.fillStyle = "grey";
			ctx.fillText(hariini(),20,290); 
			ctx.fillStyle = "grey";
			ctx.fillText(addDays(hariini(), -7),590,290); 
			
		}

		//mendapatkan penjualanmin dan penjualanmax
		function hariini(){
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();

			if(dd<10) {
			    dd = '0'+dd;
			} 

			if(mm<10) {
			    mm = '0'+mm;
			} 

			today = tambahnoldate(yyyy) + '-' + tambahnoldate(mm) + '-' + tambahnoldate(dd);

			return today;
		}
		//fungsi untuk mengubah yyyy-mm-dd jam-menit-detik ke yyyy-mm-dd
		function ubahkeyyyymmdd(str){
			return str.substring(0,str.indexOf(" "));
		}
		//fungsi untuk memperbaiki 4-5-2017 menjadi 04-05-2017
		function tambahnoldate(x)
		{
			str = x.toString();
			if (str.length  == 1)
			{
				str = "0" + str;
			}
			else
			{
				//alert("str length != 1 -> " + str + " length : " + str.length);
			}
			return str;
		}
		//fungsi untuk menambah date
		function addDays(date, days) {
		  var result = new Date(date);
		  result.setDate(result.getDate() + days);
		  var dd = result.getDate();
		  var mm = result.getMonth() + 1;
		  var y = result.getFullYear();


		  var someFormattedDate = tambahnoldate(y) + '-' + tambahnoldate(mm) + '-' + tambahnoldate(dd);
		  //alert(tambahnoldate(mm));
		  return someFormattedDate;
		}
		//cek koordinat mouse
		function getmousepos(canvas, evt){
			var rect = canvas.getBoundingClientRect();
			return{
			x: evt.clientX - rect.left,
			y: evt.clientY - rect.top
			};
		}
		function buatgarisgrid(){
			//gambar garis grid horizontal

			var tamp = maxtotal - mintotal;
			console.log(maxtotal + " dan " + mintotal);
			tamp = tamp / 4;
			tamp = Math.round(tamp);

			var txt = parseInt(maxtotal);
			for(var i = 1;i<=5;i++)
			{
				ctx.beginPath();
				ctx.moveTo(0,i * 50);
				ctx.lineTo(600,i*50);
				ctx.strokeStyle = "#e2e2e2";
				ctx.stroke();
				ctx.fillStyle = "black";
				ctx.fillText(txt,630,i * 50); 
				txt -= tamp;
				if(txt <0)
				{
					txt = 0;
				}
			}
		}
		

		function buatgarisgrafik(){
			//gambar garis grafik
			//urut dari yang teratas
			ctx.beginPath();

			
			for(var i = 0;i<7;i++)
			{
				//titik[i] = 300 - ((((penjualan[i]["total"] / (maxtotal - mintotal)) + mintotal) * 200) + 50);

				var tamppenjualan = parseInt(penjualan[i]["total"]);

				
				var tamppembagi = (maxtotal - mintotal);
				titik[i] = (tamppenjualan - mintotal)
				if (tamppembagi != 0)
				{
					titik[i] /= tamppembagi;
				}
				else
				{
					titik[i] = 0;
				 
				}
				titik[i] = 200 - (titik[i] * 200)  + 50; 
				

				if(i != 0)
				{

					ctx.lineTo(20 + (i*100), titik[i]);
					
				}
				else
				{
					ctx.moveTo(20, titik[i]);
				}
				//alert(titik[i]);
			}
			ctx.strokeStyle = "black";
			ctx.stroke();
		}
		
		function buatbulatangrafik(){
			//gambar bulatan
			//diameter 20px
			for(var i = 0;i<length;i++)
			{
				ctx.fillStyle = "black";
				ctx.beginPath();
	    		ctx.arc(20 + (i*100), titik[i], 5, 0, Math.PI * 2, true); // Outer circle
	    		ctx.closePath();
				ctx.fill();
			}
		}
		

		

		
		function hovermouse(evt)
		{
			var mousepos = getmousepos(canvas, evt);
			mx = mousepos.x;
			my = mousepos.y;
		}

		//BUAT ANIAMSI SAAT MOUSE HOVER DI BULATAN, MAKA MUNCUL KETERANGAN
		//lebar : 100
		//tinggi : 50
		function saatdihover(){
			for(var i = 0;i<7;i++)
			{
				

				//jika mouse bertabrakan dengan titik lingkaran
				if(mx >= (i * 100) + 10 && mx <= (i * 100) + 30)
				{
					if(my >= titik[i] - 10 && my <= titik[i] + 10)
					{
						//jika titik lingkaran berada terlalu dikiri
						var kiri = 0;
						if(mx < 100)
						{
							kiri = 100;
						}
						ctx.fillStyle="black";
						ctx.fillRect(kiri + mx - 100,my,100,50); 

						//membuat tulisan total
						var total = penjualan[i]['total'];
						if(titik[i] == 250)
						{
							total = 0;
						}
						total = "Pendapatan : " + total;

						//membuat tulisan tanggal
						var tgl = penjualan[i]['tanggal'];
						tgl = "Tgl : " + tgl;

						//draw tulisan
						ctx.fillStyle = "white";
						ctx.fillText(total,mx - 95 + kiri,my + 15); 
						ctx.fillStyle = "white";
						ctx.fillText(tgl,mx - 95 + kiri,my + 35); 
					}
				}
			}
		}
		
	</script>
	<?php
mysqli_close($kon);
?>
</html>