<!DOCTYPE html>
<html>
	<head>
		<title>Edit Meja</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master_guest.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_meja.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_meja.php">Meja</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Meja</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					if(isset($_GET['id_meja']))
					{
						$id_meja = $_GET['id_meja'];
						//mencegah sql injection
						if(is_numeric($id_meja))
						{
							$query = "select * from meja where id_meja=$id_meja";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								
								$no_meja = $hasil['no_meja'];
								$jumlah_kursi = $hasil['jumlah_kursi'];
								$status = $hasil['status'];
								
								
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID Meja : </label> <input type=\"text\" name=\"ID_meja\" value=\"$id_meja\" readonly/><br>";	
								//membuat input yang bisa diedit2

								//no_meja
								echo "<label class=\"frm\">No Meja : </label> <input type=\"text\" name=\"no_meja\" value=\"$no_meja\"/><br>";
								echo "<label class=\"frm\">Jumlah Kursi : </label> <input type=\"number\" name=\"jumlah_kursi\" value=\"$jumlah_kursi\"/><br>";

								//jumlah_kursi
								echo "<label class=\"frm\">Status : </label>";
								echo "<select name=\"status\">";

								//status
								if($status == "AVAILABLE"){
									echo "<option value=\"AVAILABLE\" selected=\"selected\">AVAILABLE</option>";
									echo "<option value=\"RESERVED\" >RESERVED</option>";
								}
								else
								{
									echo "<option value=\"AVAILABLE\" >AVAILABLE</option>";
									echo "<option value=\"RESERVED\" selected=\"selected\">RESERVED</option>";
								}
								echo "</select>";

								echo "<input type=\"hidden\" name=\"tabel\" value=\"meja\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>