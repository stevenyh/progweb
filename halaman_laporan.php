<!DOCTYPE html>
<html>
	<head>
		<title>Halaman Laporan</title> <!--ganti nama tabel-->
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script src="jquery-3.3.1.min.js"></script>
	</head>
	<body>
		<?php
			require "master.html";
			require "koneksi.php";
			
			
		?>
		<div class="content">
			
			<br>
			<div id="isi">
				<center>
					<h2>Laporan</h2>

					<table id="daftarstatistik">
						<tr>
							<td><a href="statistik_penjualan.php" class="tanpabiru"><img src="statistik_penjualan.png"><br><br><label id="judulstat">Statistik Penjualan</label></a></td>
							<td><a href="laporan_reservasi.php" class="tanpabiru"><img src="statistik_reservasi.png"><br><br><label id="judulstat">Laporan Reservasi</label></a></td>
							<td><a href="laporan_keuangan.php" class="tanpabiru"><img src="statistik_keuangan.png"><br><br><label id="judulstat">Laporan Keuangan</label></a></td>
						</tr>
						<tr>
							<td><a href="laporan_pelanggan.php" class="tanpabiru" ><img src="statistik_pelanggan.png"><br><br><label id="judulstat">Laporan Pelanggan</label></a></td>
							<td><a href="laporan_makanan.php" class="tanpabiru"><img src="statistik_makanan.png"><br><br><label id="judulstat">Laporan Makanan</label></a></td>
							<td><a href="laporan_meja.php" class="tanpabiru"><img src="statistik_meja.png"><br><br><label id="judulstat">Laporan Meja</label></a></td>
						</tr>
					</table>
				</center> 
			</div>
			
			
		</div>
	</div>
	
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
	
</html>