<!DOCTYPE html>
<html>
	<head>
		<title>Edit Keuangan</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href"gudeg_duta_keuangan.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_keuangan.php">Keuangan</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Keuangan</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					if(isset($_GET['id_transaksi']))
					{
						$id_transaksi = $_GET['id_transaksi'];
						//mencegah sql injection
						if(is_numeric($id_transaksi))
						{
							$query = "select * from keuangan where id_transaksi=$id_transaksi";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								
								$id_kategori = $hasil['id_kategori'];
								$tanggal = $hasil['tanggal'];
								$total = $hasil['total'];
								
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID Transaksi : </label> <input type=\"text\" name=\"id_transaksi\" value=\"$id_transaksi\" readonly/><br>";	
								//membuat input yang bisa diedit2
								//id_kategori
								echo "<label class=\"frm\">ID Kategori : </label>";
								echo "<select name=\"id_kategori\">";
								echo "<option disabled selected value> -- select an option -- </option>";
								$query2 = "Select id_kategori,nama_kategori from kategori_transaksi";

								$result2 = mysqli_query($kon, $query2);
								$hasil2 = array();
								while($row = mysqli_fetch_assoc($result2))
								{
									$hasil2[] = $row;
								}

								$counter2 = 0;
								foreach($hasil2 as $baris)
								{
																	
									$temp1 = $baris['id_kategori'];
									$temp2 = $baris['nama_kategori'];

									if($id_kategori == $temp1)
									{

										echo "<option value=\"$temp1\" selected=\"selected\">$temp1 - $temp2</option>";
									}
									else
									{
										echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
									}
									$counter2 += 1;
																	
								}
								echo "</select><br>";
								unset($query2);
								unset($result2);
								unset($hasil2);
								unset($counter2);

								//tanggal
								echo "<label class=\"frm\">Tanggal : </label>";
								$newdate = date('Y-m-d\TH:i', strtotime($tanggal));
								echo "<input type=\"datetime-local\" name=\"Tanggal\" value=\"$newdate\"/><br>";
								unset($newdate);

								//total
								echo "<label class=\"frm\">Total : </label> <input type=\"number\" name=\"total\" value=\"$total\"/><br>";

								echo "<input type=\"hidden\" name=\"tabel\" value=\"keuangan\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>