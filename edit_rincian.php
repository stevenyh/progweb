<!DOCTYPE html>
<html>
	<head>
		<title>Edit Rincian</title>
		<link href="style.css" rel="stylesheet" type="text/css" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php
			require "master_guest.html";
		?>
		<div class="content">
			<div class="tombolback">
				<a href="gudeg_duta_rincian.php"><img src="back_baru.png" width=70 weight=48/></a>
			</div>
			<div id="breadcrumb">

				<a href="gudeg_duta_rincian.php">Rincian</a> >  <a href="javascript:window.location.href=window.location.href">Edit</a> 
				
			</div>
			<br>
			<div id="isi">
				<h2>Edit Rincian</h2>
				
				<?php 
					if(file_exists("koneksi.php"))
					{
						require "koneksi.php";
					}
					else
					{
						echo "<h2 style=\"color : red\">File koneksi tidak ditemukan !!!</h2>";
					}
					
					
					if(isset($_GET['id_rincian']))
					{
						$id_rincian = $_GET['id_rincian'];
						//mencegah sql injection
						if(is_numeric($id_rincian))
						{
							$query = "select * from rincian where id_rincian=$id_rincian";
							$result = mysqli_query($kon, $query);
							$hasil = mysqli_fetch_assoc($result);
							if($hasil)
							{
								$id_penjualan = $hasil['id_penjualan'];
								$id_makanan = $hasil['id_makanan'];
								$kuantitas = $hasil['kuantitas'];
								
								echo "<form action=\"edit.php\" method=\"POST\">";
								//membuat input readonly (khusus ID)
								echo "<label class=\"frm\">ID Rincian : </label> <input type=\"text\" name=\"id_rincian\" value=\"$id_rincian\" readonly/><br>";	
								//membuat input yang bisa diedit2
								//id penjualan
								echo "<label class=\"frm\">ID Penjualan : </label>";
								echo "<select name=\"id_penjualan\">";
								echo "<option disabled selected value> -- select an option -- </option>";
								$query2 = "Select id_penjualan, id_pelanggan from nota_penjualan";

								$result2 = mysqli_query($kon, $query2);
								$hasil2 = array();
								while($row = mysqli_fetch_assoc($result2))
								{
									$hasil2[] = $row;
								}

								$counter2 = 0;
								foreach($hasil2 as $baris)
								{
																	
									$temp1 = $baris['id_penjualan'];
									$temp2 = $baris['id_pelanggan'];

									if($id_penjualane == $temp1)
									{

										echo "<option value=\"$temp1\" selected=\"selected\">$temp1 - $temp2</option>";
									}
									else
									{
										echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
									}
									$counter2 += 1;
																	
								}
								echo "</select><br>";
								unset($query2);
								unset($result2);
								unset($hasil2);
								unset($counter2);

								//id makanan
								echo "<label class=\"frm\">ID Makanan : </label>";
								echo "<select name=\"id_makanan\">";
								echo "<option disabled selected value> -- select an option -- </option>";
								$query2 = "Select id_makanan,nama from makanan";

								$result2 = mysqli_query($kon, $query2);
								$hasil2 = array();
								while($row = mysqli_fetch_assoc($result2))
								{
									$hasil2[] = $row;
								}

								$counter2 = 0;
								foreach($hasil2 as $baris)
								{
																	
									$temp1 = $baris['id_makanan'];
									$temp2 = $baris['nama'];

									if($id_makanan == $temp1)
									{

										echo "<option value=\"$temp1\" selected=\"selected\">$temp1 - $temp2</option>";
									}
									else
									{
										echo "<option value=\"$temp1\" >$temp1 - $temp2</option>";
									}
									$counter2 += 1;
																	
								}
								echo "</select><br>";
								unset($query2);
								unset($result2);
								unset($hasil2);
								unset($counter2);

								//kuantitas
								echo "<label class=\"frm\">Kuantitas : </label> <input type=\"number\" name=\"kuantitas\" value=\"$kuantitas\"/><br>";

								echo "<input type=\"hidden\" name=\"tabel\" value=\"rincian\"><br>";
								echo"<input type=\"submit\" value=\"simpan\"/></form>";
							}
							else
							{
								echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
							}
						}
						else
						{
							echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
						}
					}
					else
					{
						echo "<h2>Halaman yang Anda minta, SALAH !</h2>";
					}
				?>	
			</div>
		</div>
	</body>
	<?php
		require "tutupkoneksi.php";
	?>
</html>